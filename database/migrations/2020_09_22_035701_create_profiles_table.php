<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('nickname');
            $table->string('house_number');
            $table->integer('sitio_zone');
            $table->string('barangay');
            $table->string('municipality');
            $table->string('province');
            $table->string('zipcode');
            $table->integer('gender')->comment('1 = Male, 2 = Female, 3 = LGBTQ');
            $table->string('bday');
            $table->integer('civil_status')->comment('1 = Single, 2 = Married, 3 = Widowed, 4 = Common Law');
            $table->integer('occupation');
            $table->integer('blood_type');
            $table->integer('number_children');
            $table->integer('religion');
            $table->string('educational_attainment');
            $table->integer('father');
            $table->integer('mother_maiden');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
