<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residents', function (Blueprint $table) {
            $table->id();
            $table->integer('profile_id');
            $table->string('pwd');
            $table->string('medical_conditions');
            $table->integer('voter');
            $table->integer('program_4ps');
            $table->integer('solo_parent');
            $table->integer('sss');
            $table->integer('gsis');
            $table->integer('philhealt');
            $table->integer('indigent');
            $table->integer('senior_citizen');
            $table->integer('toilet_facility');
            $table->integer('house_type');
            $table->integer('water_source');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residents');
    }
}
