<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Barangay Information System</title>
    <link rel="icon" type="image/png" href="{{ asset('admin/img/logo-50x50.png') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="row p-5 m-5 text-center">
        <div class="offset-md-4 col-md-4">
            <img src="{{ asset('admin/img/logo.png') }}" alt="Logo" width="75%">
        </div>
        <div class="offset-md-3 col-md-6 my-3">
            <h2 class="text-uppercase font-weight-bold">Barangay Management System</h2>
        </div>
        <div class="offset-md-5 col-md-2 my-3">
            @guest
                <a href="/login" class="btn btn-block btn-success">LOGIN</a>
            @else
                <a href="" class="btn btn-block btn-primary">DASHBOARD</a>
            @endguest
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>