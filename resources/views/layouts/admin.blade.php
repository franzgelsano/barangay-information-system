<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="Barangay Information System is an automated profiling system aimed to consolidate the profile of the barangay's constituents.">
    <meta name="author" content="Francis Gelsano">

    <title>{{ config('app.name', 'Dashboard | Barangay Information System') }}</title>
    <link rel="icon" type="image/png" href="{{ asset('admin/img/logo-50x50.png') }}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="{{ asset('admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="{{ asset('admin/css/sb-admin-2.min.css') }}" rel="stylesheet">
</head>
<body id="page-top">
    <div id="wrapper">
        @include('admin.0-partials.sidebar')
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    @include('admin.0-partials.scroll-to-top')
    @include('admin.0-partials.logout-modal')
    
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    
    <!-- Custom scripts for all pages-->
    <script src="{{ asset('admin/js/sb-admin-2.min.js') }}"></script>
</body>
</html>